Spirent GNSS positioning and navigation testing

** Proof of Concept User Summary:**
 - ** Description: Spirent GNSS satellite simulator to evaluate receiver positioning and navigation performance with a demo Ublox receiver.


** Description: Test & Measurement Capabilities
  GNSS positioning and navigation testing

** POC Test criteria:** 
 -	Connecting GNSS receiver to simulator through a RF cable
 -	Select a GNSS scenario and run the simulation
 -	Check the key parameters from receiver to run fundamental test like TTFF, Positioning accuracy…



** POC Milestones:**
 -	Connecting GNSS receiver to simulator through a RF cable
 -	Select a GNSS scenario and run the simulation
 -	Check the key parameters from receiver to run fundamental test like TTFF, Positioning accuracy…



** Challenges:**
 -	Confirm the receiver if support the desired constellations, frequencies
 -	Better to have a RF isolated test environment



** Customer Benefits:**
 - Fully controllable, repeatable lab testing way for a GNSS receiver performance testing.
 - Save your product’s time-to-market effort


 
** Solution Elements:**
 - GSS7000 with all constellations/frequency enabled 
 - Link | [GSS7000](https://www.spirent.com/assets/u/brochure-gnss-testing-for-leo) |
